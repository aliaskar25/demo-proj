from django.urls import reverse

from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.test import APIClient


class SykaUlanNAhooy(APITestCase):
    def setUp(self):
        self.client = APIClient()
        super().setUp()

    def test_check_work(self):
        response = self.client.get('asd')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
